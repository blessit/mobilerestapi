-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2016 at 12:50 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `pwd`, `email`, `device_id`, `auth_token`, `trace_id`, `auth_time`, `status`, `db_add_date`, `db_update_date`) VALUES
(1, 'admin', '$2y$10$yaQmJTjpd0XgtNKlHNcC0OdCzHwJPdY.FVnlS.bYAfYn1eGSRMaia', 'admin@superpower.com', '131984651984651964948', '7c1d777dffa8b6d8a492152cf7b4942b', '5bxe0680680373', 1464515250, 1, '2016-05-29 09:47:30', '2016-05-29 09:54:39'),
(2, 'nick', '$2y$10$il5dEtJb7ocLlUhY4zfiaepPJpQ8BWtGgbmLVjLyqCX4FyPL4Ip9e', 'nickfury@shield.com', '06513564651681651616', '5ebacb87f4713b4bb93ff9e84ce2bc20', '7bxe7581197563', 1464515294, 1, '2016-05-29 09:48:14', '2016-05-29 10:50:21'),
(3, 'carter', '$2y$10$vi13Stowajvj6UOhNS691evuT..vcVuc/csnbNUCkH.4v3l6ppwU2', 'agentcarter@shield.com', '16456519416136165165', '1b21c7b29636e13985c5939a2a916d9c', '7bxe0371347448', 1464515359, 1, '2016-05-29 09:49:19', '2016-05-29 10:50:25'),
(4, 'steve', '$2y$10$yaywbgJpaRW3m582It3VnOx8dkyA31GlWHBtNR1Lqo0PFwtGuZxsi', 'steverogers@captainamerica.com', '16456519416136165165', 'b9d86fd30e5385aff24643e58b49b204', '2bxe1212337987', 1464515398, 1, '2016-05-29 09:49:58', '2016-05-29 10:50:31'),
(5, 'tony', '$2y$10$DjnoGjguz4g9F..jSGekPuneOGAhqyxJzkiokdtAemPo/Q5uAQX8y', 'tonystark@ironman.com', '71981916616464866', 'ac353980f8618d6d33a51f20f660b1f3', '1bxe9216297559', 1464515439, 1, '2016-05-29 09:50:39', '2016-05-29 10:50:38');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `category`, `cost`, `quantity`, `is_viewable`, `added_at`, `updated_at`, `added_by`, `last_modified_by`) VALUES
(1, 'Tessaract Cube', 'Crystalline cube-shaped containment vessel for an Infinity Stone possessing unlimited energy', 'Element', 900000000, 3, 1, '2016-05-29 09:58:54', '2016-05-29 10:35:49', 1, 1),
(2, 'Aether', 'Infinity Stone, the remnant of a singularity that has existed since before the universe began. ', 'Element', 75000000, 1, 1, '2016-05-29 09:58:54', '2016-05-29 10:35:40', 1, 1),
(3, 'Vibranium Shield', 'Similar to Captain America''s', 'Weapon', 8547112, 10, 1, '2016-05-29 10:02:57', '2016-05-29 10:02:57', 4, 4),
(4, 'Web Shooters', 'Wrist-mounted mechanical devices developed and used by Spider-Man to project synthetic webbing.', 'Weapon', 985547, 300, 1, '2016-05-29 10:02:57', '2016-05-29 10:02:57', 5, 5),
(5, 'Nitramene', 'Howard Stark''s inventions - Highly Explosive Vacuum Bomb', 'Weapon', 85000441, 100, 1, '2016-05-29 10:17:50', '2016-05-29 10:17:50', 5, 5),
(6, 'Sonic Cannon', 'Non-lethal long-range acoustic device (LRAD) weapon developed by AccuTech', 'Weapon', 9500000, 3000, 1, '2016-05-29 10:17:50', '2016-05-29 10:17:50', 5, 5),
(7, 'Ex-Wife Missile', 'Sidewinder Self-Guided Missile', 'Weapon', 23665580, 100, 1, '2016-05-29 10:17:50', '2016-05-29 10:17:50', 5, 5),
(8, 'Mjolnir', 'Similar to Thor''s Hammer. Made out if Dying Star', 'Weapon', 50000000, 4, 1, '2016-05-29 10:17:50', '2016-05-29 10:17:50', 1, 1),
(9, 'Chitauri Scepter', 'The Chitauri Scepter, sometimes referred to as Loki''s Scepter, was a scepter that served as a containment device for the Mind Stone', 'Weapon', 989741521, 1, 1, '2016-05-29 10:17:50', '2016-05-29 10:17:50', 2, 2),
(10, 'Quinjet', 'Technologically advanced S.H.I.E.L.D. jet ', 'Vehicle', 8747143, 65, 1, '2016-05-29 10:17:50', '2016-05-29 10:17:50', 3, 3),
(11, 'Lola', 'Levitating Over Land Automobile', 'Vehicle', 654174, 10, 1, '2016-05-29 10:17:50', '2016-05-29 10:17:50', 3, 3),
(12, 'M-ships', 'Class of ships used by the Ravagers.', 'Vehicle', 10252441, 600, 1, '2016-05-29 10:27:07', '2016-05-29 10:27:07', 1, 1),
(13, 'Taser Disks', 'Small weapons that can render a target unconscious by releasing electrical discharges.', 'Weapon', 5001, 20000, 1, '2016-05-29 10:27:07', '2016-05-29 10:27:07', 2, 2),
(14, 'Iridium', 'Found in meteorites, it forms anti-protons', 'Artifact', 457584, 100, 1, '2016-05-29 10:27:07', '2016-05-29 10:36:16', 3, 3),
(15, 'Quadcopter', 'Advanced quadrotor VTOL aircraft designed for the quick capture and transport ', 'Vehicle', 3252365, 100, 1, '2016-05-29 10:27:07', '2016-05-29 10:27:07', 1, 1),
(16, 'Betty', '$200,000 custom Audi Super car. Tony''s Favorite', 'Vehicle', 200001, 1, 1, '2016-05-29 10:27:07', '2016-05-29 10:27:07', 5, 5),
(17, 'Humvee', 'High Mobility Multipurpose Wheeled Vehicle', 'Vehicle', 50000, 200, 1, '2016-05-29 10:35:18', '2016-05-29 10:35:18', 2, 2),
(18, 'Railbike', 'Black Widow''s Ride', 'Vehicle', 50001, 100, 1, '2016-05-29 10:35:18', '2016-05-29 10:35:18', 2, 2),
(19, 'Pym Particles', 'Subatomic particles of an extra dimensional nature that are capable of shunting or adding mass and reducing or increasing scale of any form of matter,', 'Element', 365280, 1000, 1, '2016-05-29 10:35:18', '2016-05-29 10:35:18', 3, 3),
(20, 'Palladium', 'Can power an Arc Reactor', 'Element', 20022, 350, 1, '2016-05-29 10:35:18', '2016-05-29 10:35:18', 5, 5),
(21, 'Iso 8', 'A unique power source', 'Element', 100000, 50, 1, '2016-05-29 10:35:18', '2016-05-29 10:35:18', 4, 4),
(22, 'Diviners', 'Items constructed by the Kree thousands of years ago as a containment device for the Terrigen Crystals', 'Element', 500000, 25, 1, '2016-05-29 10:47:32', '2016-05-29 10:47:32', 3, 3),
(23, 'Kree Orb', 'Beacon created by the Kree to broadcast an emergency signal to Kree Reapers orbiting the Solar System', 'Element', 600000, 351, 1, '2016-05-29 10:47:32', '2016-05-29 10:47:32', 1, 1),
(24, 'Regeneration Cradle', 'Medical device that can heal wounds by grafting a simulacrum of organic tissue to the patient and having it bond to the patient''s cells.', 'Element', 800000, 5, 1, '2016-05-29 10:47:32', '2016-05-29 10:47:32', 5, 5),
(25, 'Toolbox', 'Compact digital storage device that holds a collected database of classified S.H.I.E.L.D. files, and additional information, privy to any acting Director of S.H.I.E.L.D.', 'Element', 7000, 6000, 1, '2016-05-29 10:47:32', '2016-05-29 10:47:32', 2, 2),
(26, 'Gravitonium', 'A rare, high atomic number element that possesses unique gravitational properties', 'Element', 1000000, 1, 1, '2016-05-29 10:47:32', '2016-05-29 10:47:32', 2, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
