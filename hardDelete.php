<?php
require('db.php');



if(isset($_POST['user_id'])) {
	$user_id = $link->real_escape_string($_POST['user_id']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please provide user id'));
	exit;
}

if(isset($_POST['delta'])) {
	$trace_id = $link->real_escape_string($_POST['delta']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Invalid request. Redirect to Login'));
	exit;
}

if(isset($_POST['token'])) {
	$token = $link->real_escape_string($_POST['token']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Invalid request. Redirect to Login'));
	exit;
}


require_once('authenticateUser.php');  // authenticates the user tokens and generates new ones if required


if(isset($_POST['product_id']) && strval($_POST['product_id']) != '') {
	$product_id = $link->real_escape_string($_POST['product_id']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please provide Product Id'));
	exit;
}


if(isset($_POST['category'])) {
	$category = trim($link->real_escape_string($_POST['category']));
} else {
	$category = '';
}

if(isset($_POST['last_seen_count']) && (is_int($_POST['last_seen_count'])) ) {
	$last_seen_count = intval($link->real_escape_string($_POST['last_seen_count']));
} else {
	$last_seen_count = 0;
}

if(isset($user_id)) {
    
   // userupdate($user_id, $link);
    $whereclause = ($category == '')?'':' AND category = "'.$category.'"';
	if(intval($user_id) == 1) //if user is admin
	{
		$delquery  = "DELETE from products WHERE id =  '".$product_id."'" ;
	}
	else //for normal users
	{
		$delquery  = "DELETE from products WHERE id =  '".$product_id."' AND added_by = '".$user_id."'" ;
	}
	$delresult = $link->query($delquery) or die('Errant query:  '.$delquery);
	require('retrieveProducts.php'); // retrives a list of products which is available (is_viewable = 1)
} else {
	$json  = array('status'=>'fail', 'message'=>'Please provide proper data',  "delta" => $newtrace_id_transfer, "token" => $token);
}

header('Content-type: application/json');
echo json_encode($json);

?>