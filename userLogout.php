<?php
require('db.php');


if(isset($_POST['user_id'])) {
	$user_id = $link->real_escape_string($_POST['user_id']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please provide user id'));
	exit;
}

if(isset($_POST['delta'])) {
	$trace_id = $link->real_escape_string($_POST['delta']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Invalid request. Redirect to Login'));
	exit;
}

if(isset($_POST['token'])) {
	$token = $link->real_escape_string($_POST['token']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Invalid request. Redirect to Login'));
	exit;
}


$json = array();
if(isset($user_id)) {
	$check_user_query = 'Select auth_token, trace_id, auth_time from users where id = "'.$user_id.'"';
	$check_user_result = $link->query($check_user_query) or die('Errant query:  '.$check_user_query);
	if (mysqli_num_rows($check_user_result)>0)
		{
			
			$newtrace_id='';
			$row    = mysqli_fetch_assoc($check_user_result);
			$trace_id_time = substr($trace_id, 10, -6);
			$diff = time() - $trace_id_time;
			$new_user_trace_id_check = strpos($trace_id,'bxe');
			$tokendiff =  time() - intval($row['auth_time']);
			
			if($token == $row['auth_token'] && $tokendiff <= 7200){
				
			
				if ($row['trace_id'] == $trace_id || ( $new_user_trace_id_check != '') || ($trace_id = '')) 
				{
					
					
					
					$logout_update_query  = "  UPDATE `".$db."`.`users` 
											  SET   trace_id = '',
											  auth_token = '',
											  auth_time = ''
											  WHERE   id = ".$user_id."
											";
											// removing tokens for current user
					$logout_update_result   = $link->query($logout_update_query) or die('Errant query:  '.$logout_update_query."<br>MySQL Error: ".$link->error);
					$link->close();
					echo json_encode(array("status"=>'success',"message"=>'Logged out Successfully', "delta" => 'logout', "token" => 'logout'));
					exit;
				}
				
				else
				{
					echo json_encode(array('status'=>'fail', 'message'=>'Invalid Request'));
					exit;	
				}
			}
			else{
				echo json_encode(array('status'=>'fail', 'message'=>'Session Expired'));
					exit;
			}

		}
		else{
			echo json_encode(array('status'=>'fail', 'message'=>'User Not found'));
				exit;
		}

} else {
	$json = array('status'=>'fail', 'message'=>'Please provide proper data', "delta" => '', "token" => '')	;
}

header('Content-type: application/json');
echo json_encode($json);

?>