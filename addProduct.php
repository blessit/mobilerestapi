<?php
require('db.php');

if(isset($_POST['user_id'])) {
	$user_id = $link->real_escape_string($_POST['user_id']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please provide user id'));
	exit;
}

if(isset($_POST['delta'])) {
	$trace_id = $link->real_escape_string($_POST['delta']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Invalid request. Redirect to Login'));
	exit;
}

if(isset($_POST['token'])) {
	$token = $link->real_escape_string($_POST['token']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Invalid request. Redirect to Login'));
	exit;
}

// product name should start with an Alphabet with minimum length as 3
if(isset($_POST['productname']) && $_POST['productname'] != '' && ctype_alpha(substr($_POST['productname'], 0, 1)) && (strlen($_POST['productname']) > 2)) {
	$productName = $link->real_escape_string($_POST['productname']); 
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Invalid product name.The name should start with an alphabet'));
	exit;
}

if(isset($_POST['description'])) {
	$productDesc = $link->real_escape_string($_POST['description']);
} else {
	$productDesc = '';
}

if(isset($_POST['quantity']) && (intval($_POST['quantity'])!=0)) {
	$productQuantity = intval($link->real_escape_string($_POST['quantity']));
} else {
	$productQuantity = 1;
}

if(isset($_POST['cost']) && (intval($_POST['cost'])!=0)) {
	$productCost = intval($link->real_escape_string($_POST['cost']));
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please Enter Valid Product Cost'));
	exit;
}

require_once('authenticateUser.php'); // authenticates the user tokens and generates new ones if required

if(isset($_POST['category'])) {
	$category = trim($link->real_escape_string($_POST['category']));
} else {
	$category = '';
}

if(isset($_POST['last_seen_count']) && (is_int($_POST['last_seen_count'])) ) {
	$last_seen_count = intval($link->real_escape_string($_POST['last_seen_count']));
} else {
	$last_seen_count = 0;
}

if(isset($user_id)) {
    

	$checkquery  = " SELECT name from products WHERE is_viewable = 1 order by id DESC";
	$checkresult = $link->query($checkquery) or die('Errant query:  '.$checkquery);
	
	$productlist = Array();
	while ($row = mysqli_fetch_assoc($checkresult)) {
		$productlist[] =  $row['name'];  
	}
	if (!in_array($productName, $productlist)) {
		$insertquery  = "INSERT INTO ".$db.".`products` (`name`, `description`, `category`, `cost`, `quantity`, `is_viewable`, `added_at`, `updated_at`, `added_by`, `last_modified_by`) VALUES ('".$productName."', '".$productDesc."', '".$category."', '".$productCost."', '".$productQuantity."', '1', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '".$user_id."', '".$user_id."')";
		
		$insertresult = $link->query($insertquery) or die('Errant query:  '.$insertquery);
		
		$last_seen_count = 0;
		require('retrieveProducts.php'); // retrives a list of products which is available (is_viewable = 1)
	}
	else{
		$json = array('status'=>'fail', 'message'=>'Product already exists', "delta" => $newtrace_id_transfer, "token" => $token);
	}
} else {
	$json = array('status'=>'fail', 'message'=>'Please provide proper data', "delta" => $newtrace_id_transfer, "token" => $token);
}

header('Content-type: application/json');
echo json_encode($json);

?>