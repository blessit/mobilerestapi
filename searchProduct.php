<?php
require('db.php');

if(isset($_POST['user_id'])) {
	$user_id = $link->real_escape_string($_POST['user_id']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please provide user id'));
	exit;
}

if(isset($_POST['delta'])) {
	$trace_id = $link->real_escape_string($_POST['delta']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Invalid request. Redirect to Login'));
	exit;
}

if(isset($_POST['token'])) {
	$token = $link->real_escape_string($_POST['token']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Invalid request. Redirect to Login'));
	exit;
}

if(isset($_POST['search_string'])) {
	$search_string = $link->real_escape_string($_POST['search_string']);
} else {
	$search_string = '';
	exit;
}

require_once('authenticateUser.php'); // authenticates the user tokens and generates new ones if required

if(isset($_POST['category'])) {
	$category = trim($link->real_escape_string($_POST['category']));
} else {
	$category = '';
}

if(isset($_POST['last_search_count']) && (is_int($_POST['last_search_count'])) ) {
	$last_search_count = intval($link->real_escape_string($_POST['last_search_count']));
} else {
	$last_search_count = 0;
}


if(isset($user_id)) {
    
    //userupdate($user_id, $link);
    $whereclause = ($category == '')?'':' AND (category = "'.$category.'")';
	
	if($last_search_count==0){	
	
		$query  = "SELECT products.id, products.name, products.description, products.category, products.cost, products.quantity, products.added_by, u1.username as 'added_user', u2.username as 'modified_user' FROM products INNER JOIN users u1 ON (products.added_by = u1.id) INNER JOIN users u2 ON (products.last_modified_by = u2.id) WHERE (products.is_viewable = 1) AND (products.name LIKE '%".$search_string."%' OR products.description LIKE '%".$search_string."%') ".$whereclause."  ORDER BY products.id DESC LIMIT 20";
		// retrives a list of products which is available (is_viewable = 1) and matches search string with name or desc of product
	
	}
	else{
		$query  = "SELECT products.id, products.name, products.description, products.category, products.cost, products.quantity, products.added_by, u1.username as 'added_user', u2.username as 'modified_user' FROM products INNER JOIN users u1 ON (products.added_by = u1.id) INNER JOIN users u2 ON (products.last_modified_by = u2.id) WHERE (products.is_viewable = 1) AND (products.name LIKE '%".$search_string."%' OR products.description LIKE '%".$search_string."%') ".$whereclause."  order by products.id desc limit ".$last_search_count.", 20";
		// retrives the next list of products which is available (is_viewable = 1) and matches search string with name or desc of product
	}
  	$result = $link->query($query) or die('Errant query:  '.$query);
	
	if($result && mysqli_num_rows($result)>0) {
		$data = array();
		$last_search_count += mysqli_num_rows($result); //updating last search count
		while ($row = mysqli_fetch_assoc($result)) {		
			$data[] = $row;				 				
		} 
		$num_rows = mysqli_num_rows($result);		
		
		$json = array("status" => 'success', "last_search"=>$last_search_count,"info" => $data,  "delta" => $newtrace_id_transfer, "token" => $token);
	} else {
		$json = array('status'=>'fail', 'message'=>'No more products',  "delta" => $newtrace_id_transfer, "token" => $token);
	}
} else {
	$json = array('status'=>'fail', 'message'=>'Please provide proper data',  "delta" => $newtrace_id_transfer, "token" => $token);
}
$link->close();
header('Content-type: application/json');
echo json_encode($json);

?>