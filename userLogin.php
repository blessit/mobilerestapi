<?php
require('db.php');


if(isset($_POST['user_input_id'])) {
	$user_cred = $link->real_escape_string($_POST['user_input_id']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please provide user name/email'));
	exit;
}

if(isset($_POST['ukey']) && ($_POST['ukey'] != '' ) ) {
	$pwd = $link->real_escape_string($_POST['ukey']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please enter password'));
	exit;
}


if(isset($_POST['device_id'])) {
	$device_id = $link->real_escape_string($_POST['device_id']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please provide IMEI'));
	exit;
}

if(isset($user_cred)) {
    
	if (!filter_var($user_cred, FILTER_VALIDATE_EMAIL)) { //login via name
		
		$check_user_query = 'SELECT id, username, pwd FROM users WHERE device_id = "'.$device_id.'" AND username = "'.$user_cred.'" AND status = 1';
		$check_user_result = $link->query($check_user_query) or die('Errant query:  '.$check_user_query);
		$trace_id_transfer = '';
		if (mysqli_num_rows($check_user_result)>0)
		{ 
			$row    = mysqli_fetch_assoc($check_user_result);
			if (password_verify($pwd, $row['pwd']))
			{
				$trace_id_part_1= randomNumber(10);
				$trace_id_part_2= time();
				$trace_id_part_3= randomNumber(6);
				
				$trace_id = $trace_id_part_1.$trace_id_part_2.$trace_id_part_3;		
				$trace_id_transfer = $trace_id_part_1.$trace_id_part_3.$trace_id_part_2;
				
				$token = bin2hex(openssl_random_pseudo_bytes(16)); //auth_token generation 
				$insertquery  = "UPDATE ".$db.".`users` SET auth_token = '".$token."' , auth_time = '".time()."', trace_id = '".$trace_id."' WHERE device_id = '".$device_id."' AND username = '".$user_cred."' ";
				;
				
				$insertresult = $link->query($insertquery) or die('Errant query:  '.$insertquery);
				$json = array('status'=>'success', 'message'=>'Logged in Successfully', "delta" => $trace_id_transfer, "token" => $token, "userno" => $row['id'] )	;
			}
			else
			{
				$json = array('status'=>'fail', 'message'=>'Incorrect Password', "delta" => '', "token" => '')	;
			}
		}
		else{
			$json = array('status'=>'fail', 'message'=>'User doesn\'t exist. Please Register first. If already registered, contact admin', "delta" => '', "token" => '')	;
		}
	   
	}
	else
	{ //login via email
		$check_user_query = 'SELECT id, username, pwd FROM users WHERE device_id = "'.$device_id.'" AND email = "'.$user_cred.'" AND status = 1';
		$check_user_result = $link->query($check_user_query) or die('Errant query:  '.$check_user_query);
		$trace_id_transfer = '';
		if (mysqli_num_rows($check_user_result)>0)
		{ 	
			$row    = mysqli_fetch_assoc($check_user_result);
			if (password_verify($pwd, $row['pwd']))
			{
				$trace_id_part_1= randomNumber(10);
				$trace_id_part_2= time();
				$trace_id_part_3= randomNumber(6);
				
				$trace_id = $trace_id_part_1.$trace_id_part_2.$trace_id_part_3;		
				$trace_id_transfer = $trace_id_part_1.$trace_id_part_3.$trace_id_part_2;
				
				$token = bin2hex(openssl_random_pseudo_bytes(16)); //auth_token generation 
				$insertquery  = "UPDATE ".$db.".`users` SET auth_token = '".$token."' , auth_time = '".time()."', trace_id = '".$trace_id."' WHERE device_id = '".$device_id."' AND email = '".$user_cred."' ";
				;
				
				$insertresult = $link->query($insertquery) or die('Errant query:  '.$insertquery);
				
				$json = array('status'=>'success', 'message'=>'Logged in Successfully', "delta" => $trace_id_transfer, "token" => $token, "userno" => $row['id'] )	;
			}
			else
			{
				$json = array('status'=>'fail', 'message'=>'Incorrect Password', "delta" => '', "token" => '', "userno" => '')	;
			}
		}
		else{
			$json = array('status'=>'fail', 'message'=>'User doesn\'t exist. Please Register first. If already registered, contact admin', "delta" => '', "userno" => '')	;
		}
	   
		
	}

} else {
	$json = array('status'=>'fail', 'message'=>'Please provide proper data', "delta" => '', "token" => '', "userno" => '')	;
}

$link->close();
	function randomNumber($length) {
		$result = '';
		for($i = 0; $i < $length; $i++) {
			$result .= mt_rand(0,9);
		}
		return $result;
	}

header('Content-type: application/json');
echo json_encode($json);

?>