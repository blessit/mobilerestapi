<?php
   $whereclause = ($category == '')?'':' AND products.category = "'.$category.'"';
	if($last_seen_count==0){	
	
		$query  = "SELECT products.id, products.name, products.description, products.category, products.cost, products.quantity, products.added_by, u1.username as 'added_user', u2.username as 'modified_user' FROM products INNER JOIN users u1 ON (products.added_by = u1.id) INNER JOIN users u2 ON (products.last_modified_by = u2.id) WHERE products.is_viewable = 1 ".$whereclause."  ORDER BY products.id DESC LIMIT 20";
		// retrives a list of products which is available (is_viewable = 1)
	}
	else{
		$query  = "SELECT products.id, products.name, products.description, products.category, products.cost, products.quantity, u1.username as 'added_user', u2.username as 'modified_user' FROM products INNER JOIN users u1 ON (products.added_by = u1.id) INNER JOIN users u2 ON (products.last_modified_by = u2.id) WHERE products.is_viewable = 1 ".$whereclause." ORDER BY products.id DESC LIMIT ".$last_seen_count.",20";
		// retrives the next list of products which is available (is_viewable = 1)
	}
  	$result = $link->query($query) or die('Errant query:  '.$query);
	
	if($result && mysqli_num_rows($result)>0) {
		$data = array();
		$last_seen_count += mysqli_num_rows($result); // //updating last seen count
		while ($row = mysqli_fetch_assoc($result)) {		
			$data[] = $row;				 				
		} 
		$num_rows = mysqli_num_rows($result);		
		
		$json = array("status" => 'success', "last_seen"=>$last_seen_count,"info" => $data,  "delta" => $newtrace_id_transfer, "token" => $token);
	} else {
		$json = array('status'=>'fail', 'message'=>'No more products',  "delta" => $newtrace_id_transfer, "token" => $token);
	}
	
	$link->close();

?>