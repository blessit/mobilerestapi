<?php
require_once('../simpletest/autorun.php');
    
class AllTests extends TestSuite {
    function AllTests() {
        parent::__construct();
        $this->addFile('registerUser_test.php');
        $this->addFile('userLogin_test.php');
		$this->addFile('getProducts_test.php');
		$this->addFile('searchProducts_test.php');
		$this->addFile('addProduct_test.php');
		$this->addFile('editProduct_test.php');
		$this->addFile('softDelete_test.php');
		$this->addFile('hardDelete_test.php');
		$this->addFile('userLogout_test.php');
		$this->addFile('completeCycle_test.php');
    }
}
?>