<?php

require_once('../simpletest/autorun.php');

class TestOfCompleteCycle extends UnitTestCase {
	
	function setUp() {
        require('../db.php');
    }
	
	
    function testcompletecycle() {

		$url = "http://localhost/wingifyassignment/registerUser.php";
		$data = array(
			'user_name' => 'Phil Coulson',
			'ukey' => '123456',
			'email_id' => 'formerdirector@shield.com',
			'device_id' => '21654843516984894'
		);
		$ch = curl_init();                    
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$output = curl_exec ($ch);
		curl_close ($ch); 
		$result =  json_decode($output);
		$this->assertTrue($result->status);
		$this->assertEqual($result->message, 'User Registered');
		
		$url = "http://localhost/wingifyassignment/userLogin.php";
		$data = array(
			'user_input_id' => 'formerdirector@shield.com',
			'ukey' => '123456',
			'device_id' => '21654843516984894'
		);
		$ch = curl_init();                    
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$output = curl_exec ($ch);
		curl_close ($ch); 
		$result =  json_decode($output);
		$this->assertTrue($result->status);
		$this->assertEqual($result->message, 'Logged in Successfully');
		
		$delta = $result->delta;
		$user_id = $result->userno;
		$token = $result->token;
		
		$url = "http://localhost/wingifyassignment/getProducts.php";
		$data = array(
			'user_id' => $user_id,
			'delta' => traceiddecoder($delta),
			'token' => $token,
			'device_id' => '21654843516984894'
		);
		$ch = curl_init();                    
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$output = curl_exec ($ch);
		curl_close ($ch); 
		$result =  json_decode($output);
		$this->assertTrue($result->status);
		$this->assertTrue($result->info);
		
		//$delta = $result->delta;
		
		$url = "http://localhost/wingifyassignment/addProduct.php";
		$data = array(
			'user_id' => $user_id,
			'delta' => traceiddecoder($delta),
			'token' => $token,
			'productname' => 'detoxyshots',
			'cost' => '920'
		);
		$ch = curl_init();                    
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$output = curl_exec ($ch);
		curl_close ($ch); 
		$result =  json_decode($output);
		$this->assertTrue($result->status);
		$this->assertTrue($result->info);
		
		//$delta = $result->delta;
		
		$url = "http://localhost/wingifyassignment/searchProduct.php";
		$data = array(
			'user_id' => $user_id,
			'delta' => traceiddecoder($delta),
			'token' => $token,
			'search_string' => 'detox',
		);
		$ch = curl_init();                    
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$output = curl_exec ($ch);
		curl_close ($ch); 
		$result =  json_decode($output);
		$this->assertTrue($result->status);
		$this->assertTrue($result->info);
		
		
		//$delta = $result->delta;
		$id = '';
		$link = new mysqli('localhost',"root","","wgasmnt");
		if ($stmt = mysqli_prepare($link, "SELECT MAX(id) FROM products")) {
			mysqli_stmt_execute($stmt);
			mysqli_stmt_bind_result($stmt, $col1);
			while (mysqli_stmt_fetch($stmt)) {
				$id = $col1;
			}
			mysqli_stmt_close($stmt);
		}
		
		
		$url = "http://localhost/wingifyassignment/editProduct.php";
		$data = array(
			'user_id' => $user_id,
			'delta' => traceiddecoder($delta),
			'token' => $token,
			'product_id' => $id,
			'productname' => 'detoxyshots v2.0'
		);
		$ch = curl_init();                    
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$output = curl_exec ($ch);
		curl_close ($ch); 
		$result =  json_decode($output);
		$this->assertTrue($result->status);
		$this->assertTrue($result->info);
		
		//$delta = $result->delta;
		
		$url = "http://localhost/wingifyassignment/softDelete.php";
		$data = array(
			'user_id' => $user_id,
			'delta' => traceiddecoder($delta),
			'token' => $token,
			'product_id' => $id
		);
		$ch = curl_init();                    
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$output = curl_exec ($ch);
		curl_close ($ch); 
		$result =  json_decode($output);
		$this->assertTrue($result->status);
		$this->assertTrue($result->info);
		
		//$delta = $result->delta;
		
		$url = "http://localhost/wingifyassignment/hardDelete.php";
		$data = array(
			'user_id' => $user_id,
			'delta' => traceiddecoder($delta),
			'token' => $token,
			'product_id' => $id
		);
		$ch = curl_init();                    
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$output = curl_exec ($ch);
		curl_close ($ch); 
		$result =  json_decode($output);
		$this->assertTrue($result->status);
		$this->assertTrue($result->info);
		
		//$delta = $result->delta;
		
		
		$url = "http://localhost/wingifyassignment/userLogout.php";
		$data = array(
			'user_id' => $user_id,
			'delta' => traceiddecoder($delta),
			'token' => $token
		);
		$ch = curl_init();                    
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$output = curl_exec ($ch);
		curl_close ($ch); 
		$result =  json_decode($output);
		$this->assertTrue($result->status);
		$this->assertEqual($result->message, 'Logged out Successfully'); 
    }
	

}

	function traceiddecoder($trace_id)
	{
		$new_user_trace_id_check = strpos($trace_id,'bxe');
		
		if( $new_user_trace_id_check == '')
		{
			$realtrace_id = substr($trace_id, 0, 10).substr($trace_id, 16).substr($trace_id, 10, -10);
			return $realtrace_id;
		}
		else
		{
			return $trace_id;
		}
		
	}



?>
