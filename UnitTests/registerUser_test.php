<?php
require('../db.php');
require_once('../simpletest/autorun.php');

class TestOfRegistering extends UnitTestCase {
    function testregisteruser() {
		$url = "http://localhost/wingifyassignment/registerUser.php";
		$data = array(
			'user_name' => 'Johny',
			'ukey' => '123456',
			'email_id' => 'johsan@red.com',
			'device_id' => '21654843516984894'
		);
		$ch = curl_init();                    
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$output = curl_exec ($ch);
		curl_close ($ch); 
		$result =  json_decode($output);
		$this->assertTrue($result->status);
    }
}


?>
