<?php
require('../db.php');
require_once('../simpletest/autorun.php');

class TestOfRetrievingProducts extends UnitTestCase {
    function testretrieveproducts() {
		$url = "http://localhost/wingifyassignment/getProducts.php";
		$data = array(
			'user_id' => '7',
			'delta' => '13519846519841981',
			'token' => '0c1d5ff9bcd9ea35a6bcdae14d262eb6',
			'device_id' => '21654843516984894'
		);
		$ch = curl_init();                    
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$output = curl_exec ($ch);
		curl_close ($ch); 
		$result =  json_decode($output);
		$this->assertTrue($result->status);
    }
}


?>
