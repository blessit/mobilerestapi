<?php
require('../db.php');
require_once('../simpletest/autorun.php');

class TestOfAddingProduct extends UnitTestCase {
    function testaddproduct() {
		$url = "http://localhost/wingifyassignment/addProduct.php";
		$data = array(
			'user_id' => '7',
			'delta' => '82177927961464286786053971',
			'token' => '0c1d5ff9bcd9ea35a6bcdae14d262eb6',
			'productname' => 'mjolnor',
			'cost' => '90'
		);
		$ch = curl_init();                    
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$output = curl_exec ($ch);
		curl_close ($ch); 
		$result =  json_decode($output);
		$this->assertTrue($result->status);
    }
}


?>
