<?php
require('db.php');


if(isset($_POST['user_name'])) {
	$user_name = $link->real_escape_string($_POST['user_name']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please provide user name'));
	exit;
}

if(isset($_POST['ukey']) && ($_POST['ukey'] != '' ) && (strlen($_POST['ukey']) > 5))  {
	$pwd = $link->real_escape_string($_POST['ukey']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please enter a valid password. Length should be at least 6 characters'));
	exit;
}


if(isset($_POST['email_id'])) {
	$email_id = $link->real_escape_string($_POST['email_id']);
	if (!filter_var($email_id, FILTER_VALIDATE_EMAIL)) {
	echo json_encode(array('status'=>'fail', 'message'=>'Please provide valid Email Id'));
	exit;
	}
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please provide Email Id'));
	exit;
}

if(isset($_POST['device_id'])) {
	$device_id = $link->real_escape_string($_POST['device_id']);
} else {
	echo json_encode(array('status'=>'fail', 'message'=>'Please provide IMEI'));
	exit;
}



require_once('authenticate.php'); //generates required tokens for new user
if(isset($user_name)) {
    
	$checkquery  = " SELECT username, email from users";
	$checkresult = $link->query($checkquery) or die('Errant query:  '.$checkquery);
	
	$userlist = Array();
	$useremaillist = Array();
	while ($row = mysqli_fetch_assoc($checkresult)) {
		$userlist[] =  $row['username'];  
		$useremaillist[] =  $row['email'];  
	}
	if (!in_array($user_name, $userlist) &&  !in_array($email_id, $useremaillist) ) { //check if user's name or email-id previously exists
		
		$token = bin2hex(openssl_random_pseudo_bytes(16)); //creating auth_token for new user
		
		$insertquery  = "INSERT INTO ".$db.".`users` (`username`, `pwd`, `email`, `device_id`, `trace_id`, `auth_time`, `auth_token`, `status`) VALUES ('".$user_name."', '".password_hash($pwd, PASSWORD_DEFAULT)."', '".$email_id."', '".$device_id."', '".$trace_id."', '".time()."', '".$token."', '1')";
		
		$insertresult = $link->query($insertquery) or die('Errant query:  '.$insertquery);
		$json= array('status'=>'success', 'message'=>'User Registered', "delta" => $trace_id_transfer);
	}
	else
	{
		$json = array('status'=>'fail', 'message'=>'Username/Email already exists, If not able using registered credentials contact admin', "delta" => '');
	}
} else {
	$json = array('status'=>'fail', 'message'=>'Please provide proper data', "delta" => '')	;
}
$link->close();
header('Content-type: application/json');
echo json_encode($json);

?>